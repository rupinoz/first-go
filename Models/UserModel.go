package Models

type GolangUser struct {
	Id      uint   `json:"id"`
	Name    string `json:"name"`
	Email   string `json:"email"`
	Phone   string `json:"phone"`
	Address string `json:"address"`
}

func (b *GolangUser) TableName() string {
	return "golang_user"
}
