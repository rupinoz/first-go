package Models

type Response struct {
	Status  string      `json:"status"`
	Code    int         `json:"code"`
	Data    interface{} `json:"data"`
	Message string      `json:"message"`
}

type Response2 struct {
	Status  string `json:"status"`
	Code    string `json:"code"`
	Data    Response3
	Message string `json:"message"`
}

type Response3 struct {
	Token string `json:"userTokenAccess"`
	Exp   string `json:"webAccessExpiredTime"`
}
