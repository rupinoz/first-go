package Controllers

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func GetIndex(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"status":"Success",
		"code":http.StatusOK,
		"message":"Golang API Ready!!",
	})
}

