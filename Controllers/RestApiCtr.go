package Controllers

import (
	"encoding/json"
	"first-api/Models"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
)

func GetToken(c *gin.Context) {
	token := os.Getenv("TOKEN")
	uriToken := os.Getenv("UriToken")

	client := &http.Client{}
	req, err := http.NewRequest(http.MethodGet, uriToken, nil)
	req.Header.Add("x-api-key", token)
	resp, err := client.Do(req)

	if err != nil {
		log.Fatalln(err)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}
	var respToken Models.Response2
	err = json.Unmarshal(body, &respToken)
	if err != nil {
		log.Fatalln(err)
	}

	fmt.Println("======")
	fmt.Println(uriToken)
	fmt.Println(string(body))
	fmt.Println(resp.Header)
	fmt.Println(resp.Status)
	fmt.Println("====== ")

	if resp.StatusCode == http.StatusOK {
		response := Models.Response{Status: "Success", Code: http.StatusOK, Data: respToken.Data.Token, Message: "Get Token Success"}
		c.JSON(http.StatusOK, response)
	} else {
		response := Models.Response{Status: "Failed", Code: http.StatusBadRequest, Message: respToken.Message}
		c.JSON(http.StatusBadRequest, response)
	}

}
