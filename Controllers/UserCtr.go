package Controllers

import (
	"first-api/Models"
	"first-api/Repository"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

//GetUsers ... Get all users
func GetUsers(c *gin.Context) {
	var user []Models.GolangUser
	err := Repository.GetAllUsers(&user)
	if err != nil {
		c.AbortWithStatus(http.StatusNotFound)
	} else {
		resp := Models.Response{Status: "Success", Code: http.StatusOK, Data: user}
		c.JSON(http.StatusOK, resp)
	}
}

//CreateUser ... Create User
func CreateUser(c *gin.Context) {
	var user Models.GolangUser
	c.BindJSON(&user)
	err := Repository.CreateUser(&user)
	if err != nil {
		fmt.Println(err.Error())
		c.AbortWithStatus(http.StatusNotFound)
	} else {
		resp := Models.Response{Status: "Success", Code: http.StatusOK, Data: user}
		c.JSON(http.StatusOK, resp)
	}
}

//GetUserByID ... Get the user by id
func GetUserByID(c *gin.Context) {
	id := c.Params.ByName("id")
	var user Models.GolangUser
	err := Repository.GetUserByID(&user, id)
	if err != nil {
		c.AbortWithStatus(http.StatusNotFound)
	} else {
		resp := Models.Response{Status: "Success", Code: http.StatusOK, Data: user}
		c.JSON(http.StatusOK, resp)
	}
}

//UpdateUser ... Update the user information
func UpdateUser(c *gin.Context) {
	var user Models.GolangUser
	id := c.Params.ByName("id")
	err := Repository.GetUserByID(&user, id)
	if err != nil {
		c.JSON(http.StatusNotFound, user)
	}
	c.BindJSON(&user)
	err = Repository.UpdateUser(&user, id)
	if err != nil {
		c.AbortWithStatus(http.StatusNotFound)
	} else {
		resp := Models.Response{Status: "Success", Code: http.StatusOK, Data: user}
		c.JSON(http.StatusOK, resp)
	}
}

//DeleteUser ... Delete the user
func DeleteUser(c *gin.Context) {
	var user Models.GolangUser
	id := c.Params.ByName("id")
	err := Repository.DeleteUser(&user, id)
	if err != nil {
		c.AbortWithStatus(http.StatusNotFound)
	} else {
		resp := Models.Response{Status: "Success", Code: http.StatusOK, Data: "Id " + id + " is deleted"}
		c.JSON(http.StatusOK, resp)
	}
}
