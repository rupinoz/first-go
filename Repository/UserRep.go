package Repository

import (
	"first-api/Config"
	"first-api/Models"
	"fmt"

	_ "github.com/go-sql-driver/mysql"
)

//GetAllUsers Fetch all user data
func GetAllUsers(user *[]Models.GolangUser) (err error) {
	if err = Config.DB.Find(user).Error; err != nil {
		return err
	}
	return nil
}

//CreateUser ... Insert New data
func CreateUser(user *Models.GolangUser) (err error) {
	if err = Config.DB.Create(user).Error; err != nil {
		return err
	}
	return nil
}

//GetUserByID ... Fetch only one user by Id
func GetUserByID(user *Models.GolangUser, id string) (err error) {
	if err = Config.DB.Where("id = ?", id).First(user).Error; err != nil {
		return err
	}
	return nil
}

//UpdateUser ... Update user
func UpdateUser(user *Models.GolangUser, id string) (err error) {
	fmt.Println(user)
	Config.DB.Where("id = ?", id).Save(user)
	return nil
}

//DeleteUser ... Delete user
func DeleteUser(user *Models.GolangUser, id string) (err error) {
	Config.DB.Where("id = ?", id).Delete(user)
	return nil
}
